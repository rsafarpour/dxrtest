
struct payload_t {
    float3 colour;
};

[shader("anyhit")]
void main(inout payload_t payload, BuiltInTriangleIntersectionAttributes attrib) {
    payload.colour = float3(1.0f, 1.0f, 0.0f);
}