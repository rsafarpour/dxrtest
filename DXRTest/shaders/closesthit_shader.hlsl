
struct payload_t {
    float3 colour;
};

struct Vertex {
    float3 position;
    float3 colour;
};

StructuredBuffer<Vertex> vertices : register(t1);

[shader("closesthit")]
void main(inout payload_t payload, BuiltInTriangleIntersectionAttributes attrib) {

    // BuiltInTriangleIntersectionAttributes:
    // barycentrics.x - Barycentric weight for the 2nd triangle vertex
    // barycentrics.y - Barycentric weight for the 3rd triangle vertex
    uint prim_id = PrimitiveIndex();
    payload.colour = vertices[prim_id * 3 + 0].colour * (1.0f - attrib.barycentrics.x - attrib.barycentrics.y)
                   + vertices[prim_id * 3 + 1].colour * attrib.barycentrics.x
                   + vertices[prim_id * 3 + 2].colour * attrib.barycentrics.y;
}