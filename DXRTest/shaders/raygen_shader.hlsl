
RWTexture2D<float4>             output : register(u0);
RaytracingAccelerationStructure as     : register(t0);

struct payload_t {
    float3 colour;
};

//------------------------------------------------------------------------

[shader("raygeneration")]
void main() {

    uint2  pixelID = DispatchRaysIndex().xy;
    float2 dims    = DispatchRaysDimensions().xy;

    float ratio = dims.x/dims.y;

    // Calculate ray direction
    float3 right = float3(1.0f, 0.0f, 0.0f);
    float3 up    = float3(0.0f, -1.0f, 0.0f);

    float2 dir = ((float2(2*pixelID) + 0.5)/dims - 1.0f) * (right + up);

    // Set up ray
    RayDesc ray = {
        float3(0.0, 0.0, 1.5),
        0.0f,
        normalize(float3(dir, -1.0f)),
        100.0f
    };
    
    // Prepare payload structure
    payload_t payload = { 0.0f, 0.0f, 0.0f };
    
    // Execute trace
    TraceRay(as,                // Acceleration structure
        RAY_FLAG_FORCE_OPAQUE,  // Ray flags
        0xff,                   // Instance mask
        0,                      // Ray type
        1,                      // Number of ray types
        0,                      // Miss shader index
        ray,                    // Ray description
        payload);               // Custom payload
    
    // Evaluate result
    output[pixelID] = float4(payload.colour, 1.0f);
    //output[pixelID] = float4(1.0f, 0.0f, 0.0f, 1.0f);
}