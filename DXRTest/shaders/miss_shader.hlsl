
struct payload_t {
    float3 colour;
};

[shader("miss")]
void main(inout payload_t payload) {
    payload.colour = float3(0.0f, 0.0f, 0.0f);
}