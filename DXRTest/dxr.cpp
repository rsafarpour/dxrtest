#include "dxr.h"
#include "output.h"
#include "shader_compile.h"

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")

ID3D12Resource* create_scratchbuffer(DXR* dxr, uint64_t size) {

    D3D12_HEAP_PROPERTIES properties = {};
    properties.Type                 = D3D12_HEAP_TYPE_DEFAULT;
    properties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    properties.CreationNodeMask     = 0;
    properties.VisibleNodeMask      = 0;
    properties.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;

    D3D12_RESOURCE_DESC desc = {};
    desc.Dimension           = D3D12_RESOURCE_DIMENSION_BUFFER;
    desc.Alignment           = 0;
    desc.Width               = size;
    desc.Height              = 1;
    desc.DepthOrArraySize    = 1;
    desc.MipLevels           = 1;
    desc.Format              = DXGI_FORMAT_UNKNOWN;
    desc.SampleDesc          = { 1, 0 };
    desc.Layout              = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    desc.Flags               = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

    ID3D12Resource* resource = nullptr;
    if (FAILED(dxr->device->CreateCommittedResource(
        &properties,
        D3D12_HEAP_FLAG_NONE,
        &desc,
        D3D12_RESOURCE_STATE_COMMON,
        nullptr,
        IID_PPV_ARGS(&resource)))) {
        return nullptr;
    }
    
    return resource;
}

ID3D12Resource* create_uploadheap(DXR* dxr, uint64_t size) {

    D3D12_HEAP_PROPERTIES prop_uploadheap = {};
    prop_uploadheap.Type                 = D3D12_HEAP_TYPE_UPLOAD;
    prop_uploadheap.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    prop_uploadheap.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    prop_uploadheap.CreationNodeMask     = 0;
    prop_uploadheap.VisibleNodeMask      = 0;
    
    D3D12_RESOURCE_DESC desc_instancedesc = {};
    desc_instancedesc.Dimension        = D3D12_RESOURCE_DIMENSION_BUFFER;
    desc_instancedesc.Alignment        = 0;
    desc_instancedesc.Width            = size;
    desc_instancedesc.Height           = 1;
    desc_instancedesc.DepthOrArraySize = 1;
    desc_instancedesc.MipLevels        = 1;
    desc_instancedesc.Format           = DXGI_FORMAT_UNKNOWN;
    desc_instancedesc.SampleDesc       = { 1, 0 };
    desc_instancedesc.Layout           = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    desc_instancedesc.Flags            = D3D12_RESOURCE_FLAG_NONE;


    ID3D12Resource* out = nullptr;
    if (FAILED(dxr->device->CreateCommittedResource(
        &prop_uploadheap,
        D3D12_HEAP_FLAG_NONE,
        &desc_instancedesc,
        D3D12_RESOURCE_STATE_GENERIC_READ,
        nullptr,
        IID_PPV_ARGS(&out))))
    {
        return nullptr;
    }

    return out;
}

ID3D12Resource* create_ASbuffer(DXR* dxr, uint64_t size) {

    D3D12_HEAP_PROPERTIES properties = {};
    properties.Type                 = D3D12_HEAP_TYPE_DEFAULT;
    properties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    properties.CreationNodeMask     = 0;
    properties.VisibleNodeMask      = 0;
    properties.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;

    D3D12_RESOURCE_DESC desc = {};
    desc.Dimension           = D3D12_RESOURCE_DIMENSION_BUFFER;
    desc.Alignment           = 0;
    desc.Width               = size;
    desc.Height              = 1;
    desc.DepthOrArraySize    = 1;
    desc.MipLevels           = 1;
    desc.Format              = DXGI_FORMAT_UNKNOWN;
    desc.SampleDesc          = { 1, 0 };
    desc.Layout              = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    desc.Flags               = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

    ID3D12Resource* resource = nullptr;
    if (FAILED(dxr->device->CreateCommittedResource(
        &properties,
        D3D12_HEAP_FLAG_NONE,
        &desc,
        D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE,
        nullptr,
        IID_PPV_ARGS(&resource)))) {
        return nullptr;
    }

    return resource;
}

IDXGIAdapter* find_adapter(IDXGIFactory* dxgi) {

    UINT i_adapter = -1;
    IDXGIAdapter* adapter;
    while (SUCCEEDED(dxgi->EnumAdapters(++i_adapter, &adapter))) {

        DXGI_ADAPTER_DESC desc_adapter = {};
        if (FAILED(adapter->GetDesc(&desc_adapter))) {
            return nullptr;
        }

        print_info(L"Found adapter \"%s\"\n", desc_adapter.Description);
        if(desc_adapter.DedicatedVideoMemory > 0) {
            print_info(L"Selecting adapter \"%s\" as it is the first dedicated GPU that has been detected\n", desc_adapter.Description);
            return adapter;
        }

        adapter->Release();
    }
    
    return nullptr;
}

ID3D12Device5* setup_device(IDXGIFactory* dxgi) {

    IDXGIAdapter* adapter = find_adapter(dxgi);
    if (adapter == nullptr) {
        return nullptr;
    }
    
    ID3D12Device5* device = nullptr;
    if (FAILED(D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_12_1, IID_PPV_ARGS(&device)))) {
        device = nullptr;
    }
    

    return device;
}

bool supports_raytracing(ID3D12Device* device) {
    D3D12_FEATURE_DATA_D3D12_OPTIONS5 features5 = {};
    if (FAILED(device->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &features5, sizeof(features5)))) {
        return false;
    }

    if (features5.RaytracingTier == D3D12_RAYTRACING_TIER_NOT_SUPPORTED) {
        return false;
    }

    return true;
}

ID3D12CommandQueue* setup_queue(ID3D12Device* device) {
    D3D12_COMMAND_QUEUE_DESC desc_queue = {};
    desc_queue.Type     = D3D12_COMMAND_LIST_TYPE_DIRECT;
    desc_queue.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
    desc_queue.Flags    = D3D12_COMMAND_QUEUE_FLAG_NONE;
    desc_queue.NodeMask = 0x00;

    ID3D12CommandQueue* queue;
    if(FAILED(device->CreateCommandQueue(&desc_queue, IID_PPV_ARGS(&queue)))) {
        return nullptr;
    }

    return queue;
}

ID3D12CommandAllocator* setup_allocator(ID3D12Device* device) {
    ID3D12CommandAllocator* allocator = nullptr;
    if (FAILED(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,
        IID_PPV_ARGS(&allocator)))) {

        return nullptr;
    }

    return allocator;
}

ID3D12GraphicsCommandList4* setup_cmdlist(ID3D12Device* device, ID3D12CommandAllocator* allocator) {
    ID3D12GraphicsCommandList4* cmdlist = nullptr;
    if (FAILED(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, allocator, nullptr, IID_PPV_ARGS(&cmdlist)))) {
        return nullptr;
    }

    cmdlist->Close();

    return cmdlist;
}

IDXGISwapChain3* setup_swapchain(IDXGIFactory* dxgi, DXR* dxr, Window* window) {

    DXGI_SWAP_CHAIN_DESC desc_sc = {};
    desc_sc.BufferDesc.Width            = window->width;
    desc_sc.BufferDesc.Height           = window->height;
    desc_sc.BufferDesc.RefreshRate      = {1, 60};
    desc_sc.BufferDesc.Format           = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc_sc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
    desc_sc.BufferDesc.Scaling          = DXGI_MODE_SCALING_UNSPECIFIED;
    desc_sc.SampleDesc                  = {1, 0};
    desc_sc.BufferUsage                 = DXGI_USAGE_BACK_BUFFER;
    desc_sc.BufferCount                 = 2;
    desc_sc.OutputWindow                = window->hwnd;
    desc_sc.Windowed                    = TRUE;
    desc_sc.SwapEffect                  = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    desc_sc.Flags                       = 0;

    IDXGISwapChain* swap_chain;
    if (FAILED(dxgi->CreateSwapChain(dxr->queue, &desc_sc, &swap_chain))) {
        return nullptr;
    }

    IDXGISwapChain3* out;
    if (FAILED(swap_chain->QueryInterface(&out))) {
        swap_chain->Release();
        return nullptr;
    }

    swap_chain->Release();
    return out;
}

void build_as(DXR* dxr, Geometry* g) {

    D3D12_RAYTRACING_GEOMETRY_DESC desc_geometry = {};
    desc_geometry.Type                                 = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
    desc_geometry.Triangles.VertexBuffer.StartAddress  = g->buffer_vertices->GetGPUVirtualAddress();
    desc_geometry.Triangles.VertexBuffer.StrideInBytes = g->stride_vertex;
    desc_geometry.Triangles.VertexCount                = g->n_vertices;
    desc_geometry.Triangles.VertexFormat               = DXGI_FORMAT_R32G32B32_FLOAT;
    desc_geometry.Triangles.IndexBuffer                = g->buffer_indices->GetGPUVirtualAddress();
    desc_geometry.Triangles.IndexCount                 = g->n_indices;
    desc_geometry.Triangles.IndexFormat                = DXGI_FORMAT_R32_UINT;
    desc_geometry.Triangles.Transform3x4               = 0;
    desc_geometry.Flags                                = D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE;

    D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS inputs = {};
    inputs.DescsLayout    = D3D12_ELEMENTS_LAYOUT_ARRAY;
    inputs.Flags          = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
    inputs.NumDescs       = 1;
    inputs.Type           = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
    inputs.pGeometryDescs = &desc_geometry;

    D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO prebuild_info;
    dxr->device->GetRaytracingAccelerationStructurePrebuildInfo(&inputs, &prebuild_info);

    ID3D12Resource* scratch_blas = create_scratchbuffer(dxr, prebuild_info.ScratchDataSizeInBytes);
    ID3D12Resource* blas         = create_ASbuffer(dxr, prebuild_info.ResultDataMaxSizeInBytes);
    blas->SetName(L"BOTTOM LEVEL ACCELERATION STRUCTURE");

    if (scratch_blas == nullptr) { blas->Release(); return; }
    if (blas         == nullptr) { scratch_blas->Release(); return; }

    D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC desc_blas = {};
    desc_blas.DestAccelerationStructureData = blas->GetGPUVirtualAddress();
    desc_blas.Inputs = inputs;
    desc_blas.ScratchAccelerationStructureData = scratch_blas->GetGPUVirtualAddress();

    dxr->cmdlist->Reset(dxr->allocator, nullptr);
    dxr->cmdlist->BuildRaytracingAccelerationStructure(&desc_blas, 0, nullptr);
    dxr->cmdlist->Close();

    ID3D12CommandList* list[] = { dxr->cmdlist };
    dxr->queue->ExecuteCommandLists(1, list);
    
    wait_for_flush(dxr);

    // Free resources no longer needed:
    scratch_blas->Release();

//  -------------------------------------------------------------------

    D3D12_RAYTRACING_INSTANCE_DESC desc_instance = {};
    desc_instance.Transform[0][0]                     = 1.0f;
    desc_instance.Transform[0][1]                     = 0.0f;
    desc_instance.Transform[0][2]                     = 0.0f;
    desc_instance.Transform[0][3]                     = 0.0f;
    desc_instance.Transform[1][0]                     = 0.0f;
    desc_instance.Transform[1][1]                     = 1.0f;
    desc_instance.Transform[1][2]                     = 0.0f;
    desc_instance.Transform[1][3]                     = 0.0f;
    desc_instance.Transform[2][0]                     = 0.0f;
    desc_instance.Transform[2][1]                     = 0.0f;
    desc_instance.Transform[2][2]                     = 1.0f;
    desc_instance.Transform[2][3]                     = 0.0f;
    desc_instance.InstanceID                          = 0;
    desc_instance.InstanceMask                        = 0x01;
    desc_instance.InstanceContributionToHitGroupIndex = 0;
    desc_instance.Flags                               = D3D12_RAYTRACING_INSTANCE_FLAG_NONE;
    desc_instance.AccelerationStructure               = blas->GetGPUVirtualAddress();

    ID3D12Resource* buffer_instancedesc = create_uploadheap(dxr, sizeof(desc_instance));

    void* dst = nullptr;
    buffer_instancedesc->Map(0, nullptr, &dst);
    memcpy(dst, &desc_instance, sizeof(desc_instance));
    buffer_instancedesc->Unmap(0, nullptr);

    D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS inputs_tlas = {};
    inputs_tlas.Type          = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
    inputs_tlas.InstanceDescs = buffer_instancedesc->GetGPUVirtualAddress();
    inputs_tlas.NumDescs      = 1;
    inputs_tlas.Flags         = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
    inputs_tlas.DescsLayout   = D3D12_ELEMENTS_LAYOUT_ARRAY;

    D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO info_tlas = {};
    dxr->device->GetRaytracingAccelerationStructurePrebuildInfo(&inputs_tlas, &info_tlas);

    ID3D12Resource* scratch_tlas = create_scratchbuffer(dxr, info_tlas.ScratchDataSizeInBytes);
    ID3D12Resource* tlas = create_ASbuffer(dxr, info_tlas.ResultDataMaxSizeInBytes);
    tlas->SetName(L"TOP LEVEL ACCELERATION STRUCTURE");

    if (scratch_blas == nullptr) { blas->Release(); return; }
    if (blas         == nullptr) { scratch_blas->Release(); return; }

    D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC tlas_desc = {};
    tlas_desc.Inputs                           = inputs_tlas;
    tlas_desc.DestAccelerationStructureData    = tlas->GetGPUVirtualAddress();
    tlas_desc.ScratchAccelerationStructureData = scratch_tlas->GetGPUVirtualAddress();

    dxr->cmdlist->Reset(dxr->allocator, nullptr);
    dxr->cmdlist->BuildRaytracingAccelerationStructure(&tlas_desc, 0, nullptr);
    //dxr->cmdlist->ResourceBarrier(1, &desc_barrier);
    dxr->cmdlist->Close();
    dxr->queue->ExecuteCommandLists(1, list);
    
    wait_for_flush(dxr);

    scratch_tlas->Release();
    buffer_instancedesc->Release();

    g->blas = blas;
    g->tlas = tlas;
}

ID3D12RootSignature* create_localrootsignature(DXR* dxr) {

    D3D12_DESCRIPTOR_RANGE uav_range = {};
    uav_range.BaseShaderRegister                = 0;
    uav_range.NumDescriptors                    = 1;
    uav_range.OffsetInDescriptorsFromTableStart = 0;
    uav_range.RangeType                         = D3D12_DESCRIPTOR_RANGE_TYPE_UAV;
    uav_range.RegisterSpace                     = 0;

    // This range contains the acceleration 
    // structure and the vertex buffer
    D3D12_DESCRIPTOR_RANGE srv_range = {};
    srv_range.BaseShaderRegister                = 0;
    srv_range.NumDescriptors                    = 2;
    srv_range.OffsetInDescriptorsFromTableStart = 1;
    srv_range.RangeType                         = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
    srv_range.RegisterSpace                     = 0;

    D3D12_DESCRIPTOR_RANGE ranges[] = { uav_range, srv_range };
    D3D12_ROOT_PARAMETER desc_params = {};
    desc_params.ParameterType                       = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
    desc_params.DescriptorTable.NumDescriptorRanges = _countof(ranges);
    desc_params.DescriptorTable.pDescriptorRanges   = ranges;
    desc_params.ShaderVisibility                    = D3D12_SHADER_VISIBILITY_ALL;

    ID3D12RootSignature* signature = nullptr;
    D3D12_ROOT_SIGNATURE_DESC desc_signature;
    desc_signature.NumParameters      = 1;
    desc_signature.pParameters        = &desc_params;
    desc_signature.NumStaticSamplers  = 0;
    desc_signature.pStaticSamplers    = nullptr;
    desc_signature.Flags              = D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE;

    ID3DBlob* blob_signature = nullptr;
    if (FAILED(D3D12SerializeRootSignature(&desc_signature, D3D_ROOT_SIGNATURE_VERSION_1_0, &blob_signature, nullptr))) {
        return signature;
    }

    if (FAILED(dxr->device->CreateRootSignature(0, blob_signature->GetBufferPointer(),
        blob_signature->GetBufferSize(), IID_PPV_ARGS(&signature)))) {
        signature = nullptr;
    }

    blob_signature->Release();
    return signature;
}

DXR dxr_create(Window* window) {

    DXR out = {};

    if (FAILED(D3D12GetDebugInterface(IID_PPV_ARGS(&out.dbg)))) {
        dxr_kill(&out);
        return out;
    }

    out.dbg->EnableDebugLayer();

    IDXGIFactory* dxgi;
    if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&dxgi))) {
        return out;
    }
    
    out.device = setup_device(dxgi);
    if (out.device == nullptr) {
        dxr_kill(&out);
        return out;
    }
    
    if (supports_raytracing(out.device) == false) {
        dxr_kill(&out);
        return out;
    }
    
    out.queue = setup_queue(out.device);
    if (out.queue == nullptr){
        dxr_kill(&out);
        return out;
    }
    
    out.swap_chain = setup_swapchain(dxgi, &out, window);
    if (out.swap_chain == nullptr) {
        dxr_kill(&out);
        return out;
    }
    
    out.allocator = setup_allocator(out.device);
    if (out.allocator == nullptr){
        dxr_kill(&out);
        return out;
    }
    
    out.cmdlist = setup_cmdlist(out.device, out.allocator);
    if (out.cmdlist == nullptr){
        dxr_kill(&out);
        return out;
    }
    
    if (FAILED(out.device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&out.fence)))) {
        dxr_kill(&out);
        return out;
    }
    
    out.fence_val = 0;
    return out;
}

void dxr_kill(DXR* dxr) {
    if (dxr->fence      != nullptr) { dxr->fence->Release();      dxr->fence      = nullptr; }
    if (dxr->cmdlist    != nullptr) { dxr->cmdlist->Release();    dxr->cmdlist    = nullptr; }
    if (dxr->allocator  != nullptr) { dxr->allocator->Release();  dxr->allocator  = nullptr; }
    if (dxr->swap_chain != nullptr) { dxr->swap_chain->Release(); dxr->swap_chain = nullptr; }
    if (dxr->queue      != nullptr) { dxr->queue->Release();      dxr->queue      = nullptr; }
    if (dxr->device     != nullptr) { dxr->device->Release();     dxr->device     = nullptr; }
    if (dxr->dbg        != nullptr) { dxr->dbg->Release();        dxr->dbg        = nullptr; }
}

void wait_for_flush(DXR* dxr) {
    dxr->queue->Signal(dxr->fence, dxr->fence_val);

    while (dxr->fence->GetCompletedValue() < dxr->fence_val);
    ++dxr->fence_val;
}


Geometry init_geometry(DXR* dxr) {

    //float vertices[] = {
    //    // --- Positions --- | ---- Normals ---- // 
    //    1.0f, -1.0f, -1.0f,    0.0f, -1.0f,  0.0f,  // [ 0]
    //    1.0f, -1.0f,  1.0f,    0.0f, -1.0f,  0.0f,  // [ 1]
    //   -1.0f, -1.0f,  1.0f,    0.0f, -1.0f,  0.0f,  // [ 2]
    //   -1.0f, -1.0f, -1.0f,    0.0f, -1.0f,  0.0f,  // [ 3]
    //                                                
    //    1.0f,  1.0f, -1.0f,    0.0f,  1.0f,  0.0f,  // [ 4]
    //   -1.0f,  1.0f, -1.0f,    0.0f,  1.0f,  0.0f,  // [ 5]
    //    1.0f,  1.0f,  1.0f,    0.0f,  1.0f,  0.0f,  // [ 6]
    //   -1.0f,  1.0f,  1.0f,    0.0f,  1.0f,  0.0f,  // [ 7]
    //                                                
    //    1.0f, -1.0f, -1.0f,    1.0f,  0.0f,  0.0f,  // [ 8]
    //    1.0f,  1.0f, -1.0f,    1.0f,  0.0f,  0.0f,  // [ 9]
    //    1.0f,  1.0f,  1.0f,    1.0f,  0.0f,  0.0f,  // [10]
    //    1.0f, -1.0f,  1.0f,    1.0f,  0.0f,  0.0f,  // [11]
    //                                                
    //    1.0f,  1.0f,  1.0f,    0.0f,  0.0f,  1.0f,  // [12]
    //    1.0f, -1.0f,  1.0f,    0.0f,  0.0f,  1.0f,  // [13]
    //   -1.0f,  1.0f,  1.0f,    0.0f,  0.0f,  1.0f,  // [14]
    //   -1.0f, -1.0f,  1.0f,    0.0f,  0.0f,  1.0f,  // [15]
    //                                                
    //   -1.0f, -1.0f,  1.0f,   -1.0f,  0.0f,  0.0f,  // [16]
    //   -1.0f,  1.0f,  1.0f,   -1.0f,  0.0f,  0.0f,  // [17]
    //   -1.0f,  1.0f, -1.0f,   -1.0f,  0.0f,  0.0f,  // [18]
    //   -1.0f, -1.0f, -1.0f,   -1.0f,  0.0f,  0.0f,  // [19]
    //                                               
    //    1.0f,  1.0f, -1.0f,    0.0f,  0.0f, -1.0f,  // [20]
    //    1.0f, -1.0f, -1.0f,    0.0f,  0.0f, -1.0f,  // [21]
    //   -1.0f, -1.0f, -1.0f,    0.0f,  0.0f, -1.0f,  // [22]
    //   -1.0f,  1.0f, -1.0f,    0.0f,  0.0f, -1.0f   // [23]
    //};

    //uint32_t indices[] = {
    //     0,  1,  2,    0,  2,  3,
    //     4,  5,  6,    5,  7,  6,
    //     8,  9, 10,    8, 10, 11,
    //    12, 13, 14,   13, 14, 15,
    //    16, 17, 18,   16, 18, 19,
    //    20, 21, 22,   20, 22, 23
    //};

    //Geometry out;
    //out.stride_vertex = sizeof(float) * 6;
    //out.n_vertices = sizeof(vertices)/out.stride_vertex;
    //out.n_indices = sizeof(indices)/sizeof(uint32_t);

    /// DEBUG:
    float vertices[] = {
         1.0f, -1.0f, 0.0f,     1.0f, 0.0f, 0.0f,
         0.0f,  1.0f, 0.0f,     0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,     0.0f, 0.0f, 1.0f
    };

    uint32_t indices[] = { 0, 1, 2 };

    Geometry out;
    out.stride_vertex = sizeof(float) * 6;
    out.n_vertices = sizeof(vertices)/out.stride_vertex;
    out.n_indices = sizeof(indices)/sizeof(uint32_t);
    /// EOF DEBUG

    // For this example vertex and index buffer will be 
    // simple committed resources
    D3D12_RESOURCE_DESC desc_vb;
    desc_vb.Dimension        = D3D12_RESOURCE_DIMENSION_BUFFER;
    desc_vb.Alignment        = 0;
    desc_vb.Width            = sizeof(vertices);
    desc_vb.Height           = 1;
    desc_vb.DepthOrArraySize = 1;
    desc_vb.MipLevels        = 1;
    desc_vb.Format           = DXGI_FORMAT_UNKNOWN;
    desc_vb.SampleDesc       = { 1, 0 };
    desc_vb.Layout           = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    desc_vb.Flags            = D3D12_RESOURCE_FLAG_NONE;

    D3D12_RESOURCE_DESC desc_ib;
    desc_ib.Dimension        = D3D12_RESOURCE_DIMENSION_BUFFER;
    desc_ib.Alignment        = 0;
    desc_ib.Width            = sizeof(indices);
    desc_ib.Height           = 1;
    desc_ib.DepthOrArraySize = 1;
    desc_ib.MipLevels        = 1;
    desc_ib.Format           = DXGI_FORMAT_UNKNOWN;
    desc_ib.SampleDesc       = { 1, 0 };
    desc_ib.Layout           = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    desc_ib.Flags            = D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;

    D3D12_HEAP_PROPERTIES prop_bufferheap = {};
    prop_bufferheap.Type                 = D3D12_HEAP_TYPE_DEFAULT;
    prop_bufferheap.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    prop_bufferheap.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    prop_bufferheap.CreationNodeMask     = 0;
    prop_bufferheap.VisibleNodeMask      = 0;

    if (FAILED(dxr->device->CreateCommittedResource(
        &prop_bufferheap,
        D3D12_HEAP_FLAG_NONE,
        &desc_vb, 
        D3D12_RESOURCE_STATE_COPY_DEST,
        nullptr,
        IID_PPV_ARGS(&out.buffer_vertices)))) {

        free_geometry(&out);
        return {};
    }

    out.buffer_vertices->SetName(L"VERTEX BUFFER");

    if (FAILED(dxr->device->CreateCommittedResource(
        &prop_bufferheap,
        D3D12_HEAP_FLAG_NONE,
        &desc_ib, 
        D3D12_RESOURCE_STATE_COPY_DEST,
        nullptr,
        IID_PPV_ARGS(&out.buffer_indices)))) {

        free_geometry(&out);
        return {};
    }
    out.buffer_indices->SetName(L"INDEX BUFFER");

    ID3D12Resource* upload = create_uploadheap(dxr, sizeof(vertices));
    if (upload == nullptr) { 
        free_geometry(&out);
        return {};
    }

    // Upload vertices
    void* heap_addr = nullptr;
    upload->Map(0, 0, &heap_addr);
    memcpy(heap_addr, vertices, sizeof(vertices));
    upload->Unmap(0, 0);

    dxr->cmdlist->Reset(dxr->allocator, nullptr);
    dxr->cmdlist->CopyResource(out.buffer_vertices, upload);
    dxr->cmdlist->Close();

    ID3D12CommandList* lists[] = { dxr->cmdlist };
    dxr->queue->ExecuteCommandLists(1, lists);

    wait_for_flush(dxr);

    // Upload indices
    heap_addr = nullptr;
    upload->Map(0, 0, &heap_addr);
    memcpy(heap_addr, indices, sizeof(indices));
    upload->Unmap(0, 0);


    D3D12_RESOURCE_BARRIER barriers[2];
    barriers[0].Type                   = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barriers[0].Flags                  = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    barriers[0].Transition.pResource   = out.buffer_vertices;
    barriers[0].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
    barriers[0].Transition.StateAfter  = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
    barriers[0].Transition.Subresource = 0;
    barriers[1].Type                   = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barriers[1].Flags                  = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    barriers[1].Transition.pResource   = out.buffer_indices;
    barriers[1].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
    barriers[1].Transition.StateAfter  = D3D12_RESOURCE_STATE_INDEX_BUFFER;
    barriers[1].Transition.Subresource = 0;

    dxr->cmdlist->Reset(dxr->allocator, nullptr);
    dxr->cmdlist->CopyBufferRegion(out.buffer_indices, 0, upload, 0, sizeof(indices));
    dxr->cmdlist->ResourceBarrier(2, barriers);
    dxr->cmdlist->Close();
    dxr->queue->ExecuteCommandLists(1, lists);
    
    wait_for_flush(dxr);

    upload->Release();
    build_as(dxr, &out);

    return out;
}

void free_geometry(Geometry* g) {
    if (g->tlas            != nullptr) { g->tlas->Release(); g->tlas = nullptr; }
    if (g->blas            != nullptr) { g->blas->Release(); g->tlas = nullptr; }
    if (g->buffer_vertices != nullptr) { g->buffer_vertices->Release(); g->buffer_vertices = nullptr; }
    if (g->buffer_indices  != nullptr) { g->buffer_indices->Release();  g->buffer_indices  = nullptr; }
}

Resources create_resources(DXR* dxr, Geometry* g) {

    D3D12_HEAP_PROPERTIES prop_heap = {};
    prop_heap.Type                 = D3D12_HEAP_TYPE_DEFAULT;
    prop_heap.CPUPageProperty      = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    prop_heap.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    prop_heap.CreationNodeMask     = 0;
    prop_heap.VisibleNodeMask      = 0;

    D3D12_RESOURCE_DESC desc_resource = {};
    desc_resource.Dimension        = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    desc_resource.Alignment        = 0;
    desc_resource.Width            = 800; // TODO: Set to window resolution
    desc_resource.Height           = 600; // TODO: Set to window resolution
    desc_resource.DepthOrArraySize = 1;
    desc_resource.MipLevels        = 1;
    desc_resource.Format           = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc_resource.SampleDesc       = { 1, 0 };
    desc_resource.Layout           = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    desc_resource.Flags            = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

    ID3D12Resource* resource_rwtexture = nullptr;
    dxr->device->CreateCommittedResource(&prop_heap, D3D12_HEAP_FLAG_ALLOW_ALL_BUFFERS_AND_TEXTURES,
        &desc_resource, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, IID_PPV_ARGS(&resource_rwtexture));

    D3D12_DESCRIPTOR_HEAP_DESC desc_descrheap = {};
    desc_descrheap.Type           = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    desc_descrheap.NumDescriptors = 3;
    desc_descrheap.Flags          = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    desc_descrheap.NodeMask       = 0;

    ID3D12DescriptorHeap* descriptor_heap;
    dxr->device->CreateDescriptorHeap(&desc_descrheap, IID_PPV_ARGS(&descriptor_heap));
    
    D3D12_UNORDERED_ACCESS_VIEW_DESC desc_target = {};
    desc_target.Format               = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc_target.ViewDimension        = D3D12_UAV_DIMENSION_TEXTURE2D;
    desc_target.Texture2D.MipSlice   = 0;
    desc_target.Texture2D.PlaneSlice = 0;

    D3D12_CPU_DESCRIPTOR_HANDLE handle_descrheap = descriptor_heap->GetCPUDescriptorHandleForHeapStart();
    dxr->device->CreateUnorderedAccessView(resource_rwtexture, nullptr, &desc_target, handle_descrheap);

    D3D12_SHADER_RESOURCE_VIEW_DESC desc_accelstruct = {};
    desc_accelstruct.Format                                   = DXGI_FORMAT_UNKNOWN;
    desc_accelstruct.ViewDimension                            = D3D12_SRV_DIMENSION_RAYTRACING_ACCELERATION_STRUCTURE;
    desc_accelstruct.Shader4ComponentMapping                  = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
    desc_accelstruct.RaytracingAccelerationStructure.Location = g->tlas->GetGPUVirtualAddress();

    handle_descrheap.ptr += dxr->device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    dxr->device->CreateShaderResourceView(nullptr, &desc_accelstruct, handle_descrheap);

    D3D12_SHADER_RESOURCE_VIEW_DESC desc_vbuffer = {};
    desc_vbuffer.ViewDimension              = D3D12_SRV_DIMENSION_BUFFER;
    desc_vbuffer.Format                     = DXGI_FORMAT_UNKNOWN;
    desc_vbuffer.Shader4ComponentMapping    = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
    desc_vbuffer.Buffer.FirstElement        = 0;
    desc_vbuffer.Buffer.NumElements         = g->n_vertices;
    desc_vbuffer.Buffer.StructureByteStride = g->stride_vertex;
    desc_vbuffer.Buffer.Flags               = D3D12_BUFFER_SRV_FLAG_NONE;

    handle_descrheap.ptr += dxr->device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    dxr->device->CreateShaderResourceView(g->buffer_vertices, &desc_vbuffer, handle_descrheap);

    Resources out = {};
    out.heap   = descriptor_heap;
    out.target = resource_rwtexture;
    return out;
}

void free_resources(Resources* resources) {
    resources->heap->Release();
    resources->target->Release();
}

Pipeline create_rtpipeline(DXR* dxr) {
    Pipeline out = {};
    
    out.localroot_signature  = create_localrootsignature(dxr);
    if (out.localroot_signature == nullptr) {
        return {};
    }

    IDxcBlob* gen        = compile_shader(L"./shaders/raygen_shader.hlsl");
    IDxcBlob* miss       = compile_shader(L"./shaders/miss_shader.hlsl");
    IDxcBlob* closesthit = compile_shader(L"./shaders/closesthit_shader.hlsl");

    if (gen          == nullptr || 
        miss         == nullptr ||
        closesthit   == nullptr) {

        if (gen          != nullptr) { gen->Release();          }
        if (miss         != nullptr) { miss->Release();         }
        if (closesthit   != nullptr) { closesthit->Release();   }
        return {};
    }

    // Export shaders
    D3D12_EXPORT_DESC desc_exportgen     = {};
    desc_exportgen.ExportToRename        = L"main";
    desc_exportgen.Name                  = L"RayGenerationShader";
    desc_exportgen.Flags                 = D3D12_EXPORT_FLAG_NONE;
    D3D12_EXPORT_DESC desc_exportmiss    = {};
    desc_exportmiss.ExportToRename       = L"main";
    desc_exportmiss.Name                 = L"MissShader";
    desc_exportmiss.Flags                = D3D12_EXPORT_FLAG_NONE;
    D3D12_EXPORT_DESC desc_exportclosest = {};
    desc_exportclosest.ExportToRename    = L"main";
    desc_exportclosest.Name              = L"ClosestHitShader";
    desc_exportclosest.Flags             = D3D12_EXPORT_FLAG_NONE;

    D3D12_DXIL_LIBRARY_DESC desc_libgen         = {};
    desc_libgen.DXILLibrary.BytecodeLength      = gen->GetBufferSize();
    desc_libgen.DXILLibrary.pShaderBytecode     = gen->GetBufferPointer();
    desc_libgen.NumExports                      = 1;
    desc_libgen.pExports                        = &desc_exportgen;
    D3D12_DXIL_LIBRARY_DESC desc_libmiss        = {};
    desc_libmiss.DXILLibrary.BytecodeLength     = miss->GetBufferSize();
    desc_libmiss.DXILLibrary.pShaderBytecode    = miss->GetBufferPointer();
    desc_libmiss.NumExports                     = 1;
    desc_libmiss.pExports                       = &desc_exportmiss;
    D3D12_DXIL_LIBRARY_DESC desc_libclosest     = {};
    desc_libclosest.DXILLibrary.BytecodeLength  = closesthit->GetBufferSize();
    desc_libclosest.DXILLibrary.pShaderBytecode = closesthit->GetBufferPointer();
    desc_libclosest.NumExports                  = 1;
    desc_libclosest.pExports                    = &desc_exportclosest;

    D3D12_STATE_SUBOBJECT sub_exportgen     = {};
    sub_exportgen.Type                      = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
    sub_exportgen.pDesc                     = &desc_libgen;
    D3D12_STATE_SUBOBJECT sub_exportmiss    = {};
    sub_exportmiss.Type                     = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
    sub_exportmiss.pDesc                    = &desc_libmiss;
    D3D12_STATE_SUBOBJECT sub_exportclosest = {};
    sub_exportclosest.Type                  = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
    sub_exportclosest.pDesc                 = &desc_libclosest;





    // Create Hit Group
    D3D12_HIT_GROUP_DESC desc_hg = {};
    desc_hg.AnyHitShaderImport       = 0;
    desc_hg.ClosestHitShaderImport   = L"ClosestHitShader";
    desc_hg.IntersectionShaderImport = 0;
    desc_hg.Type                     = D3D12_HIT_GROUP_TYPE_TRIANGLES;
    desc_hg.HitGroupExport           = L"HitGroup";

    D3D12_STATE_SUBOBJECT sub_hg = {};
    sub_hg.Type  = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
    sub_hg.pDesc = &desc_hg;


    
    
    
    // Define shader config
    D3D12_RAYTRACING_SHADER_CONFIG desc_shaderconf = {};
    desc_shaderconf.MaxAttributeSizeInBytes = D3D12_RAYTRACING_MAX_ATTRIBUTE_SIZE_IN_BYTES;
    desc_shaderconf.MaxPayloadSizeInBytes   = sizeof(float)*4;

    D3D12_STATE_SUBOBJECT sub_shaderconf = {};
    sub_shaderconf.Type  = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_SHADER_CONFIG;
    sub_shaderconf.pDesc = &desc_shaderconf;





    // Establish shader config association
    const wchar_t* shaderPayloadExports[] = {
        L"RayGenerationShader", L"HitGroup"
    };


    D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION desc_shaderconfassoc = {};
    desc_shaderconfassoc.pSubobjectToAssociate = &sub_shaderconf;
    desc_shaderconfassoc.NumExports            = _countof(shaderPayloadExports);
    desc_shaderconfassoc.pExports              = shaderPayloadExports;

    D3D12_STATE_SUBOBJECT sub_shaderconfassoc = {};
    sub_shaderconfassoc.Type  = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
    sub_shaderconfassoc.pDesc = &desc_shaderconfassoc;




    // Create local root signature subobject
    D3D12_STATE_SUBOBJECT sub_localrootsignature = {};
    sub_localrootsignature.Type  = D3D12_STATE_SUBOBJECT_TYPE_LOCAL_ROOT_SIGNATURE;
    sub_localrootsignature.pDesc = &out.localroot_signature;

    // Associate shaders with local root signature
    const wchar_t* assoc_localrs[] = {
        L"RayGenerationShader", L"MissShader"
    };
    
    D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION desc_localrsassoc = {};
    desc_localrsassoc.pSubobjectToAssociate = &sub_localrootsignature;
    desc_localrsassoc.pExports              = assoc_localrs;
    desc_localrsassoc.NumExports            = _countof(assoc_localrs);

    D3D12_STATE_SUBOBJECT sub_localrootassociation = {};
    sub_localrootassociation.Type = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
    sub_localrootassociation.pDesc = &desc_localrsassoc;





    // Set up pipeline config
    D3D12_RAYTRACING_PIPELINE_CONFIG desc_pplconf = {};
    desc_pplconf.MaxTraceRecursionDepth = 1;

    D3D12_STATE_SUBOBJECT sub_pplconf = {};
    sub_pplconf.Type  = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_PIPELINE_CONFIG;
    sub_pplconf.pDesc = &desc_pplconf;

    // Assemble all subobjects into final state object
    D3D12_STATE_SUBOBJECT subs[] = { 
        sub_exportgen, sub_exportmiss, sub_exportclosest,
        sub_shaderconf, sub_hg, /*sub_shaderconfassoc,*/
        sub_localrootsignature, /*sub_globalrootsignature, */
        sub_pplconf/*, sub_localrootassociation*/ };

    D3D12_STATE_OBJECT_DESC desc_rtpso = {};
    desc_rtpso.Type          = D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE;
    desc_rtpso.NumSubobjects = _countof(subs);
    desc_rtpso.pSubobjects   = subs;

    out.rtpso = nullptr;
    if (FAILED(dxr->device->CreateStateObject(&desc_rtpso, IID_PPV_ARGS(&out.rtpso)))) {
        gen->Release();
        miss->Release();
        closesthit->Release();
        return {};
    }

    return out;
}

void free_rtpipeline(Pipeline* pipeline) {
    pipeline->localroot_signature->Release();
    pipeline->rtpso->Release();
}


ShaderTable create_shadertable(DXR* dxr, Pipeline* ppl, Resources* resources, Geometry* g) {

    // Shader records must be 64byte aligned, calculate paddings here
    uint64_t size_raygenrecord   = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES + 2*sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);
    uint64_t size_missrecord     = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
    uint64_t size_hitgrouprecord = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES + 1*sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);;
    size_raygenrecord   = (size_raygenrecord + 31) & (~31);
    size_missrecord     = (size_missrecord + 31) & (~31);
    size_hitgrouprecord = (size_hitgrouprecord + 31) & (~31);

    uint64_t addr_relmiss = (size_raygenrecord + 63) & (~63);
    uint64_t padding_miss = addr_relmiss - size_raygenrecord;


    uint64_t addr_relhitgroup = (addr_relmiss + size_missrecord + 63) & (~63);
    uint64_t padding_hitgroup = addr_relhitgroup - (addr_relmiss + size_missrecord);


    uint64_t required_size = size_raygenrecord  
        + size_missrecord
        + size_hitgrouprecord
        + padding_miss
        + padding_hitgroup;

    ID3D12StateObjectProperties* properties = nullptr;
    if (FAILED(ppl->rtpso->QueryInterface(&properties))) {
        return {};
    }

    ID3D12Resource* resource_sbt = create_uploadheap(dxr, required_size);
    if (!resource_sbt) {
        return {};
    }

    UINT size_increment = dxr->device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    D3D12_GPU_DESCRIPTOR_HANDLE handle_target      = resources->heap->GetGPUDescriptorHandleForHeapStart();
    D3D12_GPU_DESCRIPTOR_HANDLE handle_accelstruct = {};
    D3D12_GPU_DESCRIPTOR_HANDLE handle_vbuffer     = {};
    handle_accelstruct.ptr                         = handle_target.ptr + size_increment;
    handle_vbuffer.ptr                             = handle_accelstruct.ptr + size_increment;

    uint32_t offset_miss = size_raygenrecord + padding_miss;
    uint32_t offset_hg   = offset_miss + size_missrecord + padding_hitgroup;
    char* mem_sbt = nullptr;
    if (SUCCEEDED(resource_sbt->Map(0, nullptr, (void**)&mem_sbt))) {

        char* start = mem_sbt;
        memcpy(mem_sbt, properties->GetShaderIdentifier(L"RayGenerationShader"), sizeof(void*));
        mem_sbt += D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;

        *reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(mem_sbt) = handle_target;
        mem_sbt += sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);
        *reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(mem_sbt) = handle_accelstruct;
        mem_sbt += sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);

        mem_sbt = start + offset_miss;
        memcpy(mem_sbt, properties->GetShaderIdentifier(L"MissShader"), sizeof(void*));
        mem_sbt += D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;

        mem_sbt = start + offset_hg;
        memcpy(mem_sbt, properties->GetShaderIdentifier(L"HitGroup"), sizeof(void*));
        mem_sbt += D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;

        mem_sbt += sizeof(D3D12_GPU_DESCRIPTOR_HANDLE); // Unused RW texture
        mem_sbt += sizeof(D3D12_GPU_DESCRIPTOR_HANDLE); // Unused acceleration structure

        *reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(mem_sbt) = handle_vbuffer;
        mem_sbt += sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);

        resource_sbt->Unmap(0, nullptr);
    }
    properties->Release();

    wait_for_flush(dxr);

    ShaderTable st = {};
    st.table = resource_sbt;
    st.offset_missshader = offset_miss;
    st.offset_hitgroups  = offset_hg;
    return st;
}

void free_shadertable(ShaderTable* st) {
    if (st->table != nullptr) { st->table->Release(); st->table = nullptr; }
}

void dispatch_rays(DXR* dxr, Pipeline* ppl, ShaderTable* st, Resources* resources, Geometry* g) {

    uint32_t raygen_recordsize   = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES + 2*sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);
    uint32_t miss_recordsize     = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
    uint32_t hitgroup_recordsize = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES + 1*sizeof(D3D12_GPU_DESCRIPTOR_HANDLE);

    // Sizes must be multiple of 32
    raygen_recordsize   = (raygen_recordsize   + 31) & (~31);
    miss_recordsize     = (miss_recordsize     + 31) & (~31);
    hitgroup_recordsize = (hitgroup_recordsize + 31) & (~31);

    D3D12_DISPATCH_RAYS_DESC desc_dispatch = {};
    desc_dispatch.RayGenerationShaderRecord.StartAddress = st->table->GetGPUVirtualAddress();
    desc_dispatch.RayGenerationShaderRecord.SizeInBytes  = raygen_recordsize;
    desc_dispatch.MissShaderTable.StartAddress           = st->table->GetGPUVirtualAddress() + st->offset_missshader;
    desc_dispatch.MissShaderTable.SizeInBytes            = miss_recordsize;
    desc_dispatch.MissShaderTable.StrideInBytes          = miss_recordsize;
    desc_dispatch.HitGroupTable.StartAddress             = st->table->GetGPUVirtualAddress() + st->offset_hitgroups;
    desc_dispatch.HitGroupTable.SizeInBytes              = hitgroup_recordsize;
    desc_dispatch.HitGroupTable.StrideInBytes            = hitgroup_recordsize;
    desc_dispatch.CallableShaderTable                    = {};
    desc_dispatch.Width                                  = 800; // TODO: Adjust to window size
    desc_dispatch.Height                                 = 600; // TODO: Adjust to window size
    desc_dispatch.Depth                                  = 1;

    dxr->cmdlist->Reset(dxr->allocator, nullptr);
    dxr->cmdlist->SetDescriptorHeaps(1, &resources->heap);
    dxr->cmdlist->SetPipelineState1(ppl->rtpso);
    dxr->cmdlist->DispatchRays(&desc_dispatch);
    dxr->cmdlist->Close();

    ID3D12CommandList* list[] = { dxr->cmdlist };
    dxr->queue->ExecuteCommandLists(1, list);

    wait_for_flush(dxr);
}

void copy_output(DXR* dxr, Resources* resources) {

    ID3D12Resource* backbuffer;
    if (FAILED(dxr->swap_chain->GetBuffer(dxr->swap_chain->GetCurrentBackBufferIndex(), IID_PPV_ARGS(&backbuffer)))) {
        return;
    }

    D3D12_RESOURCE_BARRIER to_cpysrc = {};
    to_cpysrc.Type                   = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    to_cpysrc.Transition.pResource   = resources->target;
    to_cpysrc.Transition.StateBefore = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
    to_cpysrc.Transition.StateAfter  = D3D12_RESOURCE_STATE_COPY_SOURCE;
    to_cpysrc.Transition.Subresource = 0;

    D3D12_RESOURCE_BARRIER to_cpydst = {};
    to_cpydst.Type                   = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    to_cpydst.Transition.pResource   = backbuffer;
    to_cpydst.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
    to_cpydst.Transition.StateAfter  = D3D12_RESOURCE_STATE_COPY_DEST;
    to_cpydst.Transition.Subresource = 0;

    D3D12_RESOURCE_BARRIER to_rt = to_cpysrc;
    to_rt.Transition.StateAfter  = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
    to_rt.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_SOURCE;

    D3D12_RESOURCE_BARRIER to_uacc = to_cpydst;
    to_uacc.Transition.StateAfter  = D3D12_RESOURCE_STATE_PRESENT;
    to_uacc.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;

    D3D12_RESOURCE_BARRIER before[] = { to_cpysrc, to_cpydst };
    D3D12_RESOURCE_BARRIER after[]  = { to_rt, to_uacc};

    dxr->cmdlist->Reset(dxr->allocator, nullptr);
    dxr->cmdlist->ResourceBarrier(2, before);
    dxr->cmdlist->CopyResource(backbuffer, resources->target);
    dxr->cmdlist->ResourceBarrier(2, after);
    dxr->cmdlist->Close();

    ID3D12CommandList* list[] = { dxr->cmdlist }; 
    dxr->queue->ExecuteCommandLists(1, list);

    wait_for_flush(dxr);

    backbuffer->Release();

    if (FAILED(dxr->allocator->Reset())) {
        OutputDebugString(L"Allocator issue, couldn't reset");
    }
}

void present(DXR* dxr) {
    dxr->swap_chain->Present(1, 0);
}