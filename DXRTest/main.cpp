
#include <Windows.h>

#include "dxr.h"
#include "main_window.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
    
    Window window = open_window(hInstance, 800, 600);
    DXR dxr = dxr_create(&window);
    if (!dxr.device) {
        return -1;
    }

    Geometry g          = init_geometry(&dxr);
    Resources resources = create_resources(&dxr, &g);
    Pipeline ppl        = create_rtpipeline(&dxr);

    if (!resources.heap || !ppl.rtpso) {
        return -1;
    }

    ShaderTable st      = create_shadertable(&dxr, &ppl, &resources, &g);

    MSG msg = {};
    while (msg.message != WM_QUIT) {
        while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE) != 0 && msg.message != WM_QUIT) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        dispatch_rays(&dxr, &ppl, &st, &resources, &g);
        copy_output(&dxr, &resources);
        present(&dxr);
    }

    free_shadertable(&st);
    free_rtpipeline(&ppl);
    free_geometry(&g);
    free_resources(&resources);
    dxr_kill(&dxr);

    close_window(&window);
}