#pragma once

#define NOMINMAX
#include <Windows.h>

#include <cstdio>

template <typename... Args>
void print_error(const wchar_t* fmt, Args... args) {
    int strsize = swprintf(nullptr, 0, fmt, args...) + size_t{1};
    
    wchar_t* outstr = (wchar_t*)malloc(strsize*sizeof(wchar_t));
    swprintf(outstr, strsize, fmt, args...);
    OutputDebugString(outstr);
    free(outstr);
}

template <typename... Args>
void print_warning(const wchar_t* fmt, Args... args) {
    size_t strsize = swprintf(nullptr, 0, fmt, args...) + size_t{1};

    wchar_t* outstr = (wchar_t*)malloc(strsize*sizeof(wchar_t));
    swprintf(outstr, strsize, fmt, args...);
    OutputDebugString(outstr);
    free(outstr);
}

template <typename... Args>
void print_info(const wchar_t* fmt, Args... args) {
    size_t strsize = swprintf(nullptr, 0, fmt, args...) + size_t{1};

    wchar_t* outstr = (wchar_t*)malloc(strsize*sizeof(wchar_t));
    swprintf(outstr, strsize, fmt, args...);
    OutputDebugString(outstr);
    free(outstr);
}