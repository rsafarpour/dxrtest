#pragma once

#define NOMINMAX
#include <Windows.h>

struct Window {
    HWND        hwnd;
    int         width;
    int         height;
};

Window open_window(HINSTANCE hInstance, int width, int height);
void   close_window(Window* window);