#include "main_window.h"

#define MENUPOS_OPENFILE 0

#define ERRMSG_SETUP TEXT("Failed setting up application shell")
#define ERRCAP_SHELL TEXT("Shell Error")




LRESULT CALLBACK WndHandler(
    HWND   hwnd,
    UINT   uMsg,
    WPARAM wParam,
    LPARAM lParam) {

    switch (uMsg) {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;        
    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}

Window open_window(HINSTANCE hInstance, int width, int height) {

    WNDCLASS WndClass;
    memset(&WndClass, 0, sizeof(WndClass));

    WndClass.style         = CS_HREDRAW | CS_VREDRAW;
    WndClass.lpfnWndProc   = WndHandler;
    WndClass.hInstance     = hInstance;
    WndClass.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    WndClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
    WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    WndClass.lpszClassName = TEXT("MainWindow");
    if (RegisterClass(&WndClass) != 0) {

        HWND hwnd = CreateWindow(
            TEXT("MainWindow"),
            TEXT("TestWindow"),
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT,
            width,
            height,
            NULL,
            0,
            hInstance,
            NULL
        );

        ShowWindow(hwnd, SW_SHOW);
        
        return {
            hwnd,
            width,
            height
        };
    }

    return {};
}

void close_window(Window* window) {
    ShowWindow(window->hwnd, SW_HIDE);
    DestroyWindow(window->hwnd);
    *window = {};
}