#pragma once

#include <d3d12.h>
#include <dxgi1_4.h>
#include <inttypes.h>

#include "main_window.h"

struct ShaderTable {
    ID3D12Resource* table;
    uint32_t        offset_missshader;
    uint32_t        offset_hitgroups;
};

struct Resources {
    ID3D12Resource*       target;
    ID3D12DescriptorHeap* heap;
};

struct Geometry {
    ID3D12Resource*       buffer_vertices;
    ID3D12Resource*       buffer_indices;
    uint32_t              stride_vertex;
    uint32_t              n_vertices;
    uint32_t              n_indices;
    ID3D12Resource*       blas;
    ID3D12Resource*       tlas;
};

struct Pipeline {
    ID3D12RootSignature* localroot_signature;
    ID3D12StateObject*   rtpso;
};

struct DXR {
    IDXGISwapChain3*            swap_chain;
    ID3D12Debug*                dbg;
    ID3D12Device5*              device;
    ID3D12CommandQueue*         queue;
    ID3D12CommandAllocator*     allocator;
    ID3D12GraphicsCommandList4* cmdlist;
    ID3D12Fence*                fence;
    uint64_t                    fence_val;
};

DXR         dxr_create(Window* window);
void        dxr_kill(DXR* dxr);
void        wait_for_flush(DXR* dxr);
Geometry    init_geometry(DXR* dxr);
void        free_geometry(Geometry* g);
Resources   create_resources(DXR* dxr, Geometry* g);
void        free_resources(Resources* resources);
Pipeline    create_rtpipeline(DXR* dxr);
void        free_rtpipeline(Pipeline* pipeline);
ShaderTable create_shadertable(DXR* dxr, Pipeline* ppl, Resources* resources, Geometry* g);
void        free_shadertable(ShaderTable* st);
void        dispatch_rays(DXR* dxr, Pipeline* ppl, ShaderTable* st, Resources* resources, Geometry* g);
void        copy_output(DXR* dxr, Resources* resources);
void        present(DXR* dxr);