#pragma once

#include <d3d12.h>
#include "dxcapi.h"

IDxcBlob* compile_shader(const wchar_t* filename);
