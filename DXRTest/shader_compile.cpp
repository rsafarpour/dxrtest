#include "shader_compile.h"

#include <inttypes.h>

#pragma comment(lib, "dxcompiler.lib")

IDxcBlob* compile_shader(const wchar_t* filename) {
    
    IDxcCompiler2* compiler;
    IDxcLibrary*   library;
    if (FAILED(DxcCreateInstance(CLSID_DxcCompiler, IID_PPV_ARGS(&compiler)))) {
        return nullptr;
    }
    
    if (FAILED(DxcCreateInstance(CLSID_DxcLibrary, IID_PPV_ARGS(&library)))) {
        compiler->Release();
        return nullptr;
    }
    


    IDxcBlobEncoding*    blob     = nullptr;
    IDxcOperationResult* result   = nullptr;
    IDxcBlob*            bytecode = nullptr;
    uint32_t             code_page = 0;
    if (SUCCEEDED(library->CreateBlobFromFile(filename, &code_page, &blob))) {
        if (SUCCEEDED(compiler->Compile(blob, filename, L"main", L"lib_6_3", nullptr, 0, nullptr, 0, nullptr, &result))) {
            
            HRESULT hr_result = 0; 
            result->GetStatus(&hr_result);

            if (hr_result != S_OK) {
                IDxcBlobEncoding* blob_error; 
                IDxcBlobEncoding* blob_errorUTF16;

                result->GetErrorBuffer(&blob_error);
                library->GetBlobAsUtf16(blob_error, &blob_errorUTF16);
                OutputDebugString((LPCWSTR)blob_errorUTF16->GetBufferPointer());

                blob_error->Release();
                blob_errorUTF16->Release();
            }
            else {
                result->GetResult(&bytecode);
            }
        }
    }

    if (result   != nullptr) { result->Release();   }
    if (blob     != nullptr) { blob->Release();     }
    if (library  != nullptr) { library->Release();  }
    if (compiler != nullptr) { compiler->Release(); }
    return bytecode;
}